import os, re, getpass
from collections import namedtuple
import procs

def get_proc():
	# Get the user's current process
	ps_proc = os.popen("ps aux")
	ps = ps_proc.read()
	ps_proc.close()
	split = [i.split(" ") for i in ps.split("\n") if i!=""]
	split2 = [[j for j in i if j!=""] for i in split]
	
	processes = []
	for line in split2:
		if(special_cases(line)):
			argv = [i for i in line[10:] if i[0]!="-"]
			if(argv):
				processes.append(argv)
	
	for item in procs.whitelist:
		for proc in processes:
			if(os.path.basename(proc[0]) == item.name):
				return item(argv=proc)

	return procs.default()

def special_cases(line):
	# Check if the process is just our own process, etc
	P = namedtuple("P",["user","pid","cpu","mem","vsz","rss","tty","stat","start","time","command"])(*line[:10],line[10:])
	if(P.user != getpass.getuser()):
		return False
	if(P.pid == str(os.getpid())):
		return False
	
	return True
	

if(__name__ == "__main__"):
	print(os.getpid())
	print(get_proc())
	exit(0)
