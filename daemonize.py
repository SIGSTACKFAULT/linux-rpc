#!/usr/bin/env python3
import daemon, main
with daemon.DaemonContext():
	main.main()
