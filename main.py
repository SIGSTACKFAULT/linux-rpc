#!/usr/bin/env python3
import time, re, logging

try:
	import discord_rpc as rpclib
except ImportError:
	print("Need to get RPC library.")
	print()
	print("\tgit clone https://gitlab.com/Blacksilver/discord_rpc")
	print()
	exit(1)

import get_proc

app_id = "460846935784423426"
logging.basicConfig(level=logging.INFO)

def get_rpc():
	rpc = rpclib.rpc(app_id)
	return rpc

def main():
	rpc = get_rpc()

	while True:
		try:
			proc = get_proc.get_proc()
			#print(proc)
			rpc(proc)
			time.sleep(10)
		except BrokenPipeError:
			time.sleep(1)
			rpc = get_rpc()

if(__name__ == "__main__"):
	main()
