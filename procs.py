import os,re,getpass,copy

import auto

class WhitelistItem:
	def __init__(self, name, icon="icon_bash", desc=None, path=None, large_text=None, state=None, fallback=None):
		self.name = name
		self.icon = icon
		self.state = state
		self.fallback=fallback
		
		if(desc):
			self.desc = desc
		else:
			self.desc = self.get_version()
		
		if(path):
			self.path = path
		else:
			self.path = self.get_path()
		
		if(large_text):
			self.large_text = large_text
		else:
			self.large_text = self.path
	
	def __str__(self):
		return self.name
		
	def __call__(self, argv=[]):
		actual_state = auto.user_at_host
		if(self.state):
			if(len(argv)>1):
				actual_state = self.state.format(argv=argv)
			elif(isinstance(self.fallback,list)):
				actual_state = self.state.format(argv=[self.name]+self.fallback)
			else:
				actual_state = self.fallback

		assets = {
			"large_image" : self.icon,
			"large_text" : self.large_text
		}
		
		if(auto.distro):
			assets["small_image"] = "dist_" + auto.distro.lower()
			if(auto.distro_version):
				assets["small_text"] = "{} {}".format(auto.distro.title(), auto.distro_version)
		
		return {
			"details" : self.desc,
			"state" : actual_state,
			"assets" : assets
		}
	
	def get_version(self):
		v_proc = os.popen(self.name+" --version | head -n 1")
		v_raw = v_proc.read().rstrip()
		v_proc.close()
		match = re.search("\d+\.\d+(\.\d+)?(-\w+)?", v_raw)
		if(match):
			return v_raw[:match.end()]
			
	
	def get_path(self):
		cmd_proc = os.popen("command -v "+self.name)
		path = cmd_proc.read().rstrip()
		cmd_proc.close()
		return path
	
		
whitelist = [
	WhitelistItem("vim", "icon_vim",
		state="Editing: {argv[1]}",
		fallback=["<New File>"]
	),
	WhitelistItem("gdb", "icon_gnu",
		state="Debugging: {argv[1]}"
	),
	WhitelistItem("python3", "icon_python",
		state="Script: {argv[1]}",
		fallback="<Interactive Mode>"
	),
	WhitelistItem("bash", "icon_bash")
]


default = WhitelistItem("Linux", icon="icon_tux", desc="Linux 4.15.0-23-generic", path="Tux, the Linux Penguin")
