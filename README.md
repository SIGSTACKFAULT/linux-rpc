# Linux-rpc

Shows off what you're doing on Linux in Discord rich presence.

I don't own any of the icons used; if you own the copyright on one and want it removed, open an issue.

## Run

To run in the background:

    ./daemonize.py

To run in the foreground:

    ./main.py
