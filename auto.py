import os, re
import settings

if(hasattr(settings, "username")):
	username = settings.username
else:
	try:
		from getpass import getuser
		username = getuser()
	except:
		username = "[None]"
username = username.title()


if(hasattr(settings, "hostname")):
	hostname = settings.hostname
else:
	hostname = os.uname().nodename


if(hasattr(settings, "distro")):
	distro = settings.distro
else:
	with open("/etc/os-release") as f:
		match = re.search(r'NAME=\"(.*)\"', f.read())
		distro = match.group(1)

if(hasattr(settings, "distro_version")):
	distro_version = settings.distro_version
else:
	with open("/etc/os-release") as f:
		match = re.search(r'VERSION=\"(.*)\"', f.read())
		distro_version = match.group(1)
	
user_at_host = "{}@{}".format(username, hostname)


if(__name__ == "__main__"):
	print(user_at_host)
	print(distro, distro_version)
